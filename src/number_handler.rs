use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use warp::{http::StatusCode, Filter, Rejection};

// 給予一個整數列表，請使用向量並回傳算數平均數、中位數（排序列表後正中間的值）以及眾數（出現最多次的值，雜湊映射在此應該會很有用）。

fn get_mean(number_list: &Vec<u32>) -> f32 {
    let sum: u32 = number_list.iter().sum();
    let mean: f32 = sum as f32 / number_list.len() as f32;
    mean
}

fn get_median(mut number_list: Vec<u32>) -> f32 {
    number_list.sort();
    let total = number_list.len();
    if total % 2 == 0 {
        let median = (total as u32) / 2 as u32;
        (number_list[(median - 1) as usize] as f32 + number_list[median as usize] as f32) / 2.0
    } else {
        let median_point: u32 = (total as u32) / 2;
        number_list[median_point as usize] as f32
    }
}

fn get_mode(number_list: &Vec<u32>) -> u32 {
    let map = number_list.iter().fold(HashMap::new(), |mut acc, &n| {
        let count = acc.entry(n).or_insert(0);
        *count += 1;
        return acc;
    });
    let mut mode_tup = (0, 0);
    for (num, count) in map {
        if mode_tup.1 < count {
            mode_tup.0 = num;
            mode_tup.1 = count;
        }
    }
    mode_tup.0
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct NumbersInput {
    value: Vec<u32>,
}

pub fn to_json() -> impl Filter<Extract = (NumbersInput,), Error = Rejection> + Clone {
    warp::body::content_length_limit(1024 * 16).and(warp::body::json())
}

pub async fn calculate(numbers_input: NumbersInput) -> Result<impl warp::Reply, warp::Rejection> {
    let number_list = numbers_input.value;
    let mean = get_mean(&number_list);
    let median = get_median(number_list.clone());
    let mode = get_mode(&number_list);
    Ok(warp::reply::with_status(
        format!("mean is {}, median is {}, mode is {}", mean, median, mode),
        StatusCode::OK,
    ))
}
