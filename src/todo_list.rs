use std::collections::HashMap;

// remove item
// update item
// handle route

#[derive(Debug)]
pub struct Item {
    title: String,
}

impl Item {
    fn new(title_input: &str) -> Self {
        Item {
            title: String::from(title_input),
        }
    }
}

#[derive(Debug)]
pub struct List {
    category: String,
    items: Vec<Item>,
}

impl List {
    fn new(category_input: &str) -> Self {
        List {
            category: String::from(category_input),
            items: Vec::new(),
        }
    }
    fn insert(&mut self, item: Item) -> &Self {
        self.items.push(item);
        self
    }
}

pub struct Board {
    data: HashMap<String, List>,
}

impl Board {
    fn new() -> Self {
        Board {
            data: HashMap::new(),
        }
    }
    fn categories(&self) -> Vec<&String> {
        self.data.keys().collect()
    }
    fn list(&mut self, category: &str) -> Option<&mut List> {
        self.data.get_mut(category)
    }
    fn insert_list(&mut self, category: &str) -> &mut List {
        let list_input = List::new(category);
        let key = String::from(category);
        self.data.entry(key).or_insert(list_input);
        if let Some(x) = self.data.get_mut(category) {
            x
        } else {
            panic!("insert error")
        }
    }
    fn insert_item(&mut self, category: &str, item_title: &str) -> &List {
        let list = self.insert_list(category);
        let item = Item::new(item_title);
        list.insert(item)
    }
    fn items(&mut self, category: &str) -> Option<&Vec<Item>> {
        match self.list(category) {
            Some(list) => {
                let items = &list.items;
                Some(items)
            }
            None => None,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_item_new() {
        let title = "print hello world";
        let item = Item::new(title);
        assert_eq!(item.title, title);
    }

    #[test]
    fn test_list_new() {
        let category = "codding";
        let list = List::new(category);
        assert_eq!(list.category, category);
    }

    #[test]
    fn test_list_insert() {
        let category = "codding";
        let mut list = List::new(category);
        let item_title = "print hello world";
        let item = Item::new(item_title);
        list.insert(item);
        assert_eq!(list.items[0].title, item_title);
    }

    #[test]
    fn test_create_list() {
        let mut board = Board::new();
        let category = "codding";
        let list = board.insert_list(category);
        assert_eq!(list.category, category);
    }

    #[test]
    fn test_insert_item_to_list() {
        let mut board = Board::new();
        let category = "codding";
        let item_title = "react";
        let list = board.insert_item(category, item_title);
        assert_eq!(list.items[0].title, item_title);
    }

    #[test]
    fn test_get_categories() {
        let mut board = Board::new();
        let category = "codding";
        board.insert_list(category);
        let categories = board.categories();
        assert_eq!(categories, vec![category]);
    }

    #[test]
    fn test_get_items() {
        let mut board = Board::new();
        let category = "codding";
        let item_title = "react";
        let mock_item = Item::new(item_title);
        board.insert_item(category, item_title);
        match board.items(category) {
            Some(x) => assert_eq!(x[0].title, mock_item.title),
            None => print!("Oops"),
        }
    }
}
