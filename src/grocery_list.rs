use parking_lot::RwLock;
use serde::{Deserialize, Serialize};
use std::{collections::HashMap, sync::Arc};
use warp::{http::StatusCode, Filter, Rejection};

type Items = HashMap<String, i32>;

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct Id {
    name: String,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Item {
    name: String,
    quantity: i32,
}

#[derive(Clone)]
pub struct Store {
    grocery_list: Arc<RwLock<Items>>,
}

impl Store {
    pub fn new() -> Self {
        Store {
            grocery_list: Arc::new(RwLock::new(HashMap::new())),
        }
    }
}

pub async fn update(item: Item, store: Store) -> Result<impl warp::Reply, warp::Rejection> {
    store.grocery_list.write().insert(item.name, item.quantity);
    Ok(warp::reply::with_status(
        "Added items to the grocery list",
        StatusCode::CREATED,
    ))
}

pub async fn get(store: Store) -> Result<impl warp::Reply, warp::Rejection> {
    let mut result = HashMap::new();
    let r = store.grocery_list.read();
    for (key, value) in r.iter() {
        result.insert(key, value);
    }
    Ok(warp::reply::json(&result))
}

pub async fn delete(id: Id, store: Store) -> Result<impl warp::Reply, warp::Rejection> {
    store.grocery_list.write().remove(&id.name);
    Ok(warp::reply::with_status(
        "Removed item from grocery list",
        StatusCode::OK,
    ))
}

pub fn post_json() -> impl Filter<Extract = (Item,), Error = Rejection> + Clone {
    // When accepting a body, we want a JSON body
    // (and to reject huge payloads)...
    warp::body::content_length_limit(1024 * 16).and(warp::body::json())
}

pub fn delete_json() -> impl Filter<Extract = (Id,), Error = Rejection> + Clone {
    // When accepting a body, we want a JSON body
    // (and to reject huge payloads)...
    warp::body::content_length_limit(1024 * 16).and(warp::body::json())
}
