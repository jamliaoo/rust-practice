use serde::{Deserialize, Serialize};
use warp::{http::StatusCode, Filter, Rejection};

// 將字串轉換成 pig latin。每個單字的第一個字母為子音的話，就將該字母移到單字後方，並加上「ay」，
// 所以「first」會變成「irst-fay」。而單字第一個字母為母音的話，就在單字後方加上「hay」，
// 所以「apple」會變成「apple-hay」。請注意要考慮到 UTF-8 編碼！

fn get_first_char(word: &String) -> char {
    let mut first_char: char = ' ';
    for c in word.chars() {
        first_char = c;
        break;
    }
    first_char
}

fn lowercase(char: char) -> char {
    let mut lowercase_char = char;
    for c in char.to_lowercase().to_string().chars() {
        lowercase_char = c;
        break;
    }
    lowercase_char
}

fn is_vowel(char: &char) -> bool {
    let vowels = vec!['a', 'e', 'i', 'o', 'u'];
    let result = vowels.iter().fold(false, |acc, vowel| {
        if acc == true {
            return acc;
        } else if char == vowel {
            return true;
        } else {
            return false;
        }
    });
    return result;
}

fn is_first_char_vowel(word: &String) -> bool {
    let first_char = get_first_char(&word);
    let lowercase_char = lowercase(first_char);
    return is_vowel(&lowercase_char);
}

fn separate_word_to_vectors(word: String, separate_index: usize) -> (Vec<char>, Vec<char>) {
    let mut word_vec: Vec<char> = word.chars().collect();
    let exclude_first_char_vec: Vec<char> = word_vec.drain(separate_index..).collect();
    return (word_vec, exclude_first_char_vec);
}

fn transform_by_consonant_rule(word: String) -> String {
    let (first_char_vec, rest_char_vec) = separate_word_to_vectors(word, 1);
    let mut result = String::from("");
    for v in rest_char_vec.iter() {
        result.push(v.clone())
    }
    result.push('-');
    result.push(first_char_vec[0]);
    result + "ay"
}

fn transform_by_vowel_rule(word: String) -> String {
    word + "-hey"
}

fn include_none_ascii_alphabetic(word: &String) -> bool {
    let mut result = false;
    for c in word.chars() {
        if !c.is_ascii_alphabetic() {
            result = true;
            break;
        }
    }
    result
}

fn transform(word: String) -> String {
    if include_none_ascii_alphabetic(&word) {
        return word;
    } else if is_first_char_vowel(&word) {
        return transform_by_vowel_rule(word);
    } else {
        return transform_by_consonant_rule(word);
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct WordInput {
    value: String,
}

pub fn to_json() -> impl Filter<Extract = (WordInput,), Error = Rejection> + Clone {
    warp::body::content_length_limit(1024 * 16).and(warp::body::json())
}

pub async fn to_pig_latin(word_input: WordInput) -> Result<impl warp::Reply, warp::Rejection> {
    let word = word_input.value;
    let transformed_word = transform(word);
    Ok(warp::reply::with_status(
        format!("{}", transformed_word),
        StatusCode::OK,
    ))
}
